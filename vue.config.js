const path = require('path');
const ip = require('ip');

function resolve(dir) {
    return path.join(__dirname, '.', dir);
}

module.exports = {
    publicPath: './',
    outputDir: 'dist',
    configureWebpack: {
        output: {
            sourcePrefix: ' '
        },
        amd: {
            toUrlUndefined: true
        },
        resolve: {
            alias: {
                vue$: 'vue/dist/vue.esm.js',
                '@': path.resolve('src')
            }
        },
        externals: {},
        plugins: [],
        module: {
            unknownContextCritical: false
        },
        optimization: {}
    },
    chainWebpack: config => {
        config.module
            .rule('svg')
            .exclude.add(resolve('src/icons/svg'))
            .end();

        config.module
            .rule('icons')
            .test(/\.svg$/)
            .include.add(resolve('src/icons/svg'))
            .end()
            .use('svg-sprite-loader')
            .loader('svg-sprite-loader')
            .options({ symbolId: 'icon-[name]' });
    },
    devServer: {
        // host: ip.address(),
        host: 'localhost',
        open: true, // 自动打开浏览器
        port: 8888,
        https: false,
        proxy: {
            '/Intelligent-Community': {
                target: 'http://47.97.219.219:8095',
                changeOrigin: true,
                ws: false
            }
        }
    }
};