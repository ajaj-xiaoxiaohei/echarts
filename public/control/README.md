# 向日葵 Web 主控端 SDK v2.1.1

**本文档仅作开发参考，具体功能支持情况按照相关协议而定。**

新版特性

- 可在 node.js 环境运行（详见本文档运行环境说明）
- 更丰富，友好的 API
- 新增基于 WebAssembly 的视频解码

老版本迁移指南

此版本 API 部分完全不向后兼容，但本版本依然提供完整可运行 demo 作为简单嵌入方式。对于没有进行过定制/二次开发的用户，使用 demo 目录下的 demo-sunlogin-sdk.html 页面直接替换老版本的 index.html 作为入口文件，即可平滑更新。

## 文件说明

```
├ dist
│ ├ decode.wasm // 解码器的WebAssembly二进制文件，线上需要保证与slcc-browser-wasm.js在同一路径下
│ ├ slcc-lib.js // node.js 的库文件
| ├ slcc-browser-wasm.js // 使用WebAssembly解码的浏览器端js
│ └ slcc-browser.js // 向日葵主控端浏览器环境js文件，包含本文档描述的所有API
├ demo
│ ├ demo-node.js // nodeJS环境的Demo
│ ├ demo-sunlogin-remote-client.html // 连接向日葵官方客户端Demo
│ ├ demo-sunlogin-sdk.html // 连接向日葵SDK Demo
│ ├ demo-sunlogin-wasm.html // 使用WebAssembly解码的Demo
│ ├ demo.js // html页面的UI和交互部分JS
│ └ demo.css // demo页面的样式表文件
└ README.md // 说明文档 (markdown)
```

## Demo 说明

**demo-sunlogin-sdk.html**

配合被控端 SDK 使用（或已获取连接地址的向日葵客户端），获得登录地址（Address）和会话（CID），然后用浏览器打开页面，附加从被控端获取的两个参数。

```url
demo-sunlogin-sdk.html?address=$Address&cid=$CID
```

**demo-sunlogin-wasm.html**

使用方法与 demo-sunlogin-sdk.html 相同，但因使用了 Webassembly，**必须在 http 环境下运行**，不支持直接本地环境测试。其中 decode.wasm 需要与 slcc-browser-wasm.js 放在同一个目录。

```url
demo-sunlogin-wasm.html?address=$Address&cid=$CID
```

**因依赖文件较大，相对运行要求环境要求更高（主要是浏览器版本），建议单独部署静态文件服务并合理设置缓存规则。**关于浏览器的支持情况，可以参照[这里](https://www.caniuse.com/#search=webassembly)。

**demo-sunlogin-remote-client.html**

_注意：连接标准客户端的功能只是为 Web 端开发测试过程提供便利（可以脱离被控 SDK 和服务器等环境进行开发调试）。不适合也不支持嵌入式生产环境使用，因测试的参数涉及隐私，如误用导致安全问题，责任由使用者承担。_

连接标准向日葵客户端的示例，需要获取客户端的 ID（remoteID）和主机地址域名（host），编辑文件，修改 remoteid 和 host 参数，另外需要填写相关客户端的登录验证参数。

参数的获取方法：

进入向日葵官网 >> 管理中心 >> 主机列表点击在线的向日葵主机 >> 地址栏的 URL 格式如下：

https://xxxxx-yyyyyyy.slrc.oray.com

其中 xxxxx 即为 remoteid，host 即为 xxxxx-yyyyyyy.rc03.oray.com，注意：这两个参数是您的隐私数据，请勿泄漏和传播。

**demo-node.js**

依赖 npm package

- websocket

请添加到引用项目中

```bash
npm install websocket
```

从 SDK 获取登录地址（Address 只取主机名部分）和会话（CID），执行

```bash
node ./demo/demo-node.js $host $cid
```

**注意：此 demo 执行效果是把被控端桌面的视频流保存为文件并保存在 demo 目录下，运行后将会产生大量文件。**

## 功能描述

通过本 SDK 提供的接口，可实现浏览器桌面观看/控制功能。当前可支持的客户端/SDK 包括：

- Windows 客户端 8.6.5+
- Windows SDK
- Android 客户端 3.5+
- Android SDK

demo 的 html 页面实现了完整的主控端功能，可以根据 demo 页面的注释指引进行修改即可直接使用。如有更高的定制的需求，请参照本文档 API 手册进行开发，页面上仅需引用 slcc-browser.js。

wasm 解码可以提供更好的兼容性，更低的资源占用。但对浏览器环境要求更高，同时依赖的文件较大（加载时间较长）。

## 运行环境

### Web 主控端运行环境

支持 WebSocket, Canvas（support WebGL）, 二进制 API(ArrayBuffer, DataView, TypedArray)的 Webkit 浏览器（内核环境），**由于视频解码性能原因，不建议（也不保证）移动平台浏览器使用**。

Demo demo-sunlogin-wasm.html 需要支持[WebAssembly](https://www.caniuse.com/#search=webassembly)的浏览器。

除桌面图形部分(DesktopView)外，均可在 node.js(6.10.0+)环境中运行。

### 服务器端要求（独立部署）

- 泛域名配置，Web 主控必须采用域名方式进行访问/连接服务器，不支持 IP 方式
- 浏览器运行环境下，web 主控端需要连接 80/443(如需 HTTPS)端口，服务器需要配置和开放对应端口
- 如需使用 HTTPS，需要自行配置有效的证书

**当前使用向日葵服务器，在通用浏览器环境下，不支持 HTTPS(WSS)**

## API Reference

### 包导出结构

```javascript
// replace with "window" object on browser
module.exports.SunloginControl = {
  // 核心类： Client和插件实现
  // @class Client
  // 实现客户端连接的业务
  Client: ...

  // set of plugins
  Plugin: {
    // @class Desktop
    // 远程桌面业务逻辑实现
    Desktop: ...
  }

  // 其他功能
  // @class DesktopView
  // 默认桌面视图，基于浏览器，处理&输出Desktop视频流
  // slcc-lib.js无此对象
  DesktopView: ...
}
```

详细使用示例请参见目录下的 demo 程序。

### SunloginClient.Client

向日葵主控端，客户端对象处理基本服务器连接&登录验证

SunloginClient.Client 继承了 [events.EventEmitter](https://nodejs.org/dist/latest-v8.x/docs/api/events.html#events_class_eventemitter)，详见事件列表。

#### 构造函数

SunloginControl.Client(options)

**参数列表**

| 参数名  | 类型   | 说明     |
| ------- | ------ | -------- |
| options | object | 连接设置 |

```json
// options 说明
{
  "remoteid": 0, // 主机ID|识别码，连接向日葵官方客户端需要提供，识别码前加"k"，SDK可缺省
  "ssl": true, // 是否通过SSL（https、wss）连接，服务器端必须支持且配置有效的证书
  "port": 443, // 连接端口 缺省443
  "host": null, // 连接主机地址，SDK通过生成的地址获取主机名部分
  "rpcPort": 2480, // rpc 连接端口， 缺省2480
  "udpPort": 4118, // udp端口，缺省为4118
  "embed": false, // 是否嵌入式客户端（SDK），缺省false，连接SDK请传true
  "cid": null, // 客户端的验证ID，SDK的CID，客户端不需要传入，通过客户端登陆接口获得
  "debug": false, // debug，缺省为false，true时会在浏览器控制台输出日志
  "p2pServer": "slp2p-embed01.oray.net", // 指定p2p转发服务器，标准向日葵客户端或包含服务端嵌入协议的客户根据情况传入，请勿擅自更改

  // 客户端验证参数，SDK不需要
  "auth": {
    "username": "", // 系统账号，如通过访问密码|验证码登录可省略
    "password": "" // 登录密码，如果 username 不为空，则为对应系统账号密码，否则可传访问密码|验证码
  }
}
```

**返回值**

SunloginControl.Client 的实例

#### SunloginControl.Client.connect()

根据构造函数传入的参数，发起连接。

**参数列表**

无

SunloginControl.Client 的实例

**析构指南**

如果需要断开后删除对象，让 JS 引擎 GC 回收资源，应该在 client 的 disconnect 事件的回调中实施，否则可能因闭包资源未释放而导致内存泄漏。

```
var client = new SunloginControl.Client(...);
var desktop = new SunloginControl.Plugin.Desktop(...);
var desktopview = new SunloginControl.DesktopView(...);

client
.connect()
.connectPlugin((desktop) => {
  // ...

  client.on('disconnect', () => {
    client = desktop = desktopview = null;
  });

});
```

#### SunloginControl.Client.autoConnect()

针对标准向日葵客户端，自动执行完整的连接（connect） => 登录(login) => P2P 的过程(forward)。对于 SDK 等同于 connect()方法。对于不关注连接过程的场合，建议直接使用本方法。

**参数列表**

无

**返回值**

Promise

#### SunloginControl.Client.disconnect()

断开连接，请务必保证在页面注销（unload）前关闭连接。

**参数列表**

无

**返回值**

void

#### SunloginControl.Client.login()

登录向日葵主机，登录方式根据构造函数的 options.auth 参数决定。验证不通过会立即断开连接。

**参数列表**

无

**返回值**

Promise

#### SunloginControl.Client.fastLogin()

通过主机验证码登录向日葵主机，可以通过 options.auth.password，传入验证码验证，如没有验证信息，本方法会在客户端弹出确认框，客户端点击确认通过后验证通过，否则连接会断开。

**参数列表**

无

**返回值**

Promise

#### SunloginControl.Client.forward(host)

通过指定 P2P 服务器转发，SDK 不需要

**参数列表**

| 参数名 | 类型   | 说明               |
| ------ | ------ | ------------------ |
| host   | string | 转发服务器的主机名 |

**返回值**

Promise

#### SunloginControl.Client.connectPlugin(plugin)

连接被控端插件，参数可以是 SunloginControl.Plugin.\* 中之一的实例对象，当前仅支持远程桌面(SunloginControl.Plugin.Desktop)。

**参数列表**

| 参数名 | 类型                           | 说明     |
| ------ | ------------------------------ | -------- |
| plugin | SunloginControl.Plugin.Desktop | 插件对象 |

**返回值**

Promise

#### 事件列表

| 参数名  | 回调参数    | 说明                                                               |
| ------- | ----------- | ------------------------------------------------------------------ |
| error   | error       | 插件对象                                                           |
| message | ArrayBuffer | Websocket 发生 message 事件是触发，可直接获取 Websocket 流入的数据 |

### SunloginClient.Plugin.Desktop

桌面插件实现。

SunloginClient.Plugin.Desktop 继承了 [events.EventEmitter](https://nodejs.org/dist/latest-v8.x/docs/api/events.html#events_class_eventemitter)，详见事件列表。

#### 构造函数

SunloginClient.Plugin.Desktop()

#### SunloginClient.Plugin.Desktop.switchSession(sessionId)

切换系统会话(Windows)

**参数列表**

| 参数名    | 类型 | 说明                                                                   |
| --------- | ---- | ---------------------------------------------------------------------- |
| sessionId | int  | SessionID，通过监听事件 session 可获得会话列表，其中元素包含 sessionid |

**返回值**

void

#### SunloginClient.Plugin.Desktop.switchScreen(index)

切换屏幕

**参数列表**

| 参数名 | 类型 | 说明                                                     |
| ------ | ---- | -------------------------------------------------------- |
| index  | int  | 屏幕顺序，0 为默认屏幕，屏幕数量通过监听事件 screen 获得 |

**返回值**

void

#### SunloginClient.Plugin.Desktop.sendKey(...keys)

模拟键盘事件，可通过此方法发送无法直接通过键盘输入的系统组合键，如：Ctrl + Alt + Del, Alt + F4 等等

```javascript
desktop.sendKey(17, 18, 46); // w3c keycode: ctrl + alt + delete
```

**参数列表**

| 参数名 | 类型 | 说明 |
| ------ | ---- | ---- |
| key    | int  | 键值 |

**返回值**

void

#### SunloginClient.Plugin.Desktop.transportKeyBoardEvent()

传输主控端的键盘事件（开启传输后，当前浏览器窗口在活动状态下的所有键盘事件将会被拦截，即浏览器的快捷键将会时效），本方法在 node.js 环境下不可用。

**参数列表**

无

**返回值**

void

#### SunloginClient.Plugin.Desktop.transportMouseEvent(target)

传输鼠标事件，本方法在 node.js 环境下不可用。如不传输鼠标和键盘方法，即为不可控制的观看模式。

**参数列表**

| 参数名 | 类型           | 说明                                       |
| ------ | -------------- | ------------------------------------------ |
| target | HTMLDomElement | 鼠标事件目标对象，坐标会根据该元素重新计算 |

**返回值**

void

#### 事件列表

| 参数名    | 回调参数                       | 说明                                                                                             |
| --------- | ------------------------------ | ------------------------------------------------------------------------------------------------ |
| error     | error                          | 抛出错误                                                                                         |
| imageinfo | info object                    | 客户端返回图像信息是触发                                                                         |
| stream    | stream Uint8Array, info object | H264 视频流事件，可通过此方法获取视频流数据，参数说明见：SunloginControl.DesktopView.decode      |
| session   | sessions Array                 | 返回系统会话列表是抛出（Windows)，回调参数是会话列表的数组：[{sessionid: xxx, name: "username"}] |
| screen    | screenCount int                | 返回屏幕数量时抛出，回调参数为屏幕数量                                                           |

向日葵远程桌面插件接口

### SunloginControl.DesktopView

向日葵远程桌面默认视图接口，实现解码和图像显示。本对象涉及 DOM 操作，node.js 下不可运行。

#### 构造函数

SunloginControl.DesktopView(wrapper)

**参数列表**

| 参数名  | 类型           | 说明                                                   |
| ------- | -------------- | ------------------------------------------------------ |
| wrapper | HTMLDomElement | 远程桌面显示区域父节点，最终显示内容会被添加到此元素中 |

**返回值**

SunloginControl.DesktopView 的实例

#### SunloginControl.Wasm.decode(stream, info)

对视频流进行解码并在构造函数指定的位置显示，显示元素（canvas）的尺寸根据 info 中的高宽信息决定。如需调整大小，建议通过缩放父节点（css transform）实现。

_注意：如需自行实现视频解码，Windows 客户端（SDK）的视频流需要经过额外翻转和色彩处理(if info.streamId != 0x7FFF)_

**参数列表**

| 参数名 | 类型       | 说明        |
| ------ | ---------- | ----------- |
| stream | Uint8Array | H264 视频流 |
| info   | object     | 视频信息    |

```json
{
  "streamId": 165234, // 流ID
  "width": 1024, // 视频宽度
  "height": 768, // 视频高度
  "frameId": 2991, // 桢ID,
  "length": 32123, // 视频流大小(stream.byteLength),
  "type": 0 // 保留
}
```

**返回值**

void

#### SunloginControl.Wasm.getDOMElement()

获取 DesktopView 的 DOM 节点对象(canvas)

**参数列表**

无

**返回值**

HTMLCanvasElement

### SunloginControl.Wasm

向日葵远程桌面默认视图接口，实现解码和图像显示。本对象涉及 DOM 操作，node.js 下不可运行。

#### 构造函数

SunloginControl.Wasm(wrapper)

**参数列表**

| 参数名  | 类型           | 说明                                                   |
| ------- | -------------- | ------------------------------------------------------ |
| wrapper | HTMLDomElement | 远程桌面显示区域父节点，最终显示内容会被添加到此元素中 |

**返回值**

SunloginControl.Wasm 的实例

#### SunloginControl.Wasm.decode(stream, info)

对视频流进行解码并在构造函数指定的位置显示，显示元素（canvas）的尺寸根据 info 中的高宽信息决定。如需调整大小，建议通过缩放父节点（css transform）实现。

**参数列表**

| 参数名 | 类型       | 说明        |
| ------ | ---------- | ----------- |
| stream | Uint8Array | H264 视频流 |
| info   | object     | 视频信息    |

```json
{
  "streamId": 165234, // 流ID
  "width": 1024, // 视频宽度
  "height": 768, // 视频高度
  "frameId": 2991, // 桢ID,
  "length": 32123, // 视频流大小(stream.byteLength),
  "type": 0 // 保留
}
```

**返回值**

void

#### SunloginControl.Wasm.getDOMElement()

获取视图对象的 DOM 节点对象(canvas)

**参数列表**

无

**返回值**

HTMLCanvasElement

## 常见问题

1、连接不上，浏览器控制台出现：

```
WebSocket connection to 'ws://2029.slp2p-live01.oray.net/2029' failed: Error during WebSocket handshake: Unexpected response code: 200
```

连接地址已失效，请检查被控端 SDK 状态是否正常，或是否已经重连

2、打开连接过程，发送 Host - Session 头信息后直接断开。CID 已失效，请获取最新的被控端 SDK CID 参数
