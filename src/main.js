import Vue from 'vue';
/* eslint-disable */
import ElementUI from 'element-ui';
// import 'element-ui/lib/theme-chalk/index.css';
import VueI18n from 'vue-i18n';
import zhLocale from 'element-ui/lib/locale/lang/zh-CN';
import enLocale from 'element-ui/lib/locale/lang/en';
import jaLocale from 'element-ui/lib/locale/lang/ja';
import zh from './i18n/zh';
import en from './i18n/en';
import ja from './i18n/ja';
import moment from 'moment';
import 'moment/locale/zh-cn';

import 'qweather-icons/font/qweather-icons.css';

import App from './App.vue';
import router from './router';
import store from './store';

import utils, * as utilsOther from './utils/utils';

// import './utils/rem';
import "@/utils/flexible.js";
import './components';
import './icons';
import './style/index.scss';
// import './permission.js';

Vue.config.productionTip = false
Vue.use(VueI18n)
Vue.use(utils)

moment.locale('zh-cn')
Vue.prototype.$moment = moment

const i18n = new VueI18n({
  locale: utilsOther.getLanguage(),
  messages: {
    zh: { ...zh, ...zhLocale },
    en: { ...en, ...enLocale },
    ja: { ...ja, ...jaLocale }
 }
})
window.i18n = i18n;

Vue.use(ElementUI, {
    i18n: (key, value) => i18n.t(key, value)
})

new Vue({
    data: {
        dateFormat: 'YYYY-MM-DD HH:mm:ss' // 国内YYYY-MM-DD HH:mm:ss 国外 DD/MMM/YYYY HH:mm:ss
    },
    router,
    store,
    i18n,
    render: h => h(App)
}).$mount('#app');