import http from '@/utils/sysHttp'

// 登录
function login (data) {
  return http({
    url: `/uaa/login?username=${data.username}&password=${data.password}`,
    method: 'post'
  })
}
// 登出
function logout () {
  return http({
    url: `/uaa/logout`,
    method: 'post'
  })
}
// 获取图形验证码
function getImgCode(clientId) {
  return http({
    url: `/uaa/validate/image/${clientId}`,
    method: 'get'
  })
}
// 获取首页菜单
function getUserMenus(params) {
  return http({
    url: '/api-saas/menu/getRouters',
    params: params
  })
}
// 获取菜单列表
function getMenuList(data) {
  return http({
    url: '/api-saas/menu/list',
    method: 'post',
    data
  })
}

/* 用户管理 */
// 获取用户列表
function getUserList(data) {
  return http({
    url: '/api-saas/user/list',
    method: 'post',
    data
  })
}
// 获取用户详情
function getUserInfo(data) {
  return http({
    url: '/api-saas/user/getInfo',
    method: 'post',
    data
  })
}
// 保存修改用户
function saveUser(data) {
  return http({
    url: '/api-saas/user/save',
    method: 'post',
    data
  })
}
// 删除用户
function deleteUser(data) {
  return http({
    url: '/api-saas/user/remove',
    method: 'post',
    data
  })
}
// 修改密码
function changePwd(data) {
  return http({
    url: '/api-saas/user/changePwd',
    method: 'post',
    data
  })
}
// 重置密码
function resetPwd(data) {
  return http({
    url: '/api-saas/user/resetPwd',
    method: 'post',
    data
  })
}

/* 员工管理 */
// 获取员工列表
function getEmployeeList(data) {
  return http({
    url: '/api-saas/employee/list',
    method: 'post',
    data
  })
}
// 获取员工详情
function getEmployeeInfo(data) {
  return http({
    url: '/api-saas/employee/getInfo',
    method: 'post',
    data
  })
}
// 保存员工
function saveEmployee(data) {
  return http({
    url: '/api-saas/employee/save',
    method: 'post',
    data
  })
}
// 删除员工
function deleteEmployee(data) {
  return http({
    url: '/api-saas/employee/remove',
    method: 'post',
    data
  })
}
// 导出员工
function exportEmployee(data) {
  return http({
    url: '/api-saas/employee/export',
    method: 'post',
    responseType: 'blob',
    data
  })
}
// 绑定用户
function bindUser(data) {
  return http({
    url: '/api-saas/employee/bindUser',
    method: 'post',
    data
  })
}

/* 部门管理 */
// 获取部门列表
function getDeptList(data) {
  return http({
    url: '/api-saas/dept/list',
    method: 'post',
    data
  })
}
// 获取部门详情
function getDeptInfo(data) {
  return http({
    url: '/api-saas/dept/getInfo',
    method: 'post',
    data
  })
}
// 新增部门
function addDept(data) {
  return http({
    url: '/api-saas/dept/save',
    method: 'post',
    data
  })
}
// 修改部门
function editDept(data) {
  return http({
    url: '/api-saas/dept/update',
    method: 'post',
    data
  })
}
// 删除部门
function deleteDept(data) {
  return http({
    url: '/api-saas/dept/remove',
    method: 'post',
    data
  })
}
// 获取上级部门列表
function getSuperList(data) {
  return http({
    url: '/api-saas/dept/list/exclude',
    method: 'post',
    data
  })
}

/* 角色管理 */
// 获取角色列表
function getRoleList(data) {
  return http({
    url: '/api-saas/role/list',
    method: 'post',
    data
  })
}
// 获取角色详情
function getRoleInfo(data) {
  return http({
    url: '/api-saas/role/getInfo',
    method: 'post',
    data
  })
}
// 保存角色
function saveRole(data) {
  return http({
    url: '/api-saas/role/save',
    method: 'post',
    data
  })
}
// 获取全部角色
function getAllRole(data) {
  return http({
    url: '/api-saas/role/all',
    method: 'post',
    data
  })
}
// 根据角色id查询菜单（用于角色授权勾选）
function getRoleMenu(data) {
  return http({
    url: '/api-saas/role/menu',
    method: 'post',
    data
  })
}
// 根据角色id查询部门（用于自定义数据权限勾选）
function getRoleDept(data) {
  return http({
    url: '/api-saas/role/dept',
    method: 'post',
    data
  })
}
// 删除角色
function deleteRole(data) {
  return http({
    url: '/api-saas/role/remove',
    method: 'post',
    data
  })
}
// 数据权限分配
function setAuthData(data) {
  return http({
    url: '/api-saas/role/authDataScope',
    method: 'post',
    data
  })
}

export default {
  login,
  logout,
  getImgCode,
  getUserMenus,
  getMenuList,
  getUserList,
  getUserInfo,
  saveUser,
  deleteUser,
  changePwd,
  resetPwd,
  getEmployeeList,
  getEmployeeInfo,
  saveEmployee,
  deleteEmployee,
  exportEmployee,
  bindUser,
  getDeptList,
  getDeptInfo,
  addDept,
  editDept,
  deleteDept,
  getSuperList,
  getRoleList,
  getRoleInfo,
  saveRole,
  getAllRole,
  getRoleMenu,
  getRoleDept,
  deleteRole,
  setAuthData
}
