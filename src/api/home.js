import http from '@/utils/http'

// 获取点位信息
function getEquipmentLocation() {
  return http({
    url: '/getEquipmentLocation',
    method: 'get'
  })
}

// 获取党员信息信息
function getPartyMember() {
  return http({
    url: '/getPartyMemberNum2',
    method: 'get'
  })
}

// 获取隐患记录
function getHiddenDanger() {
  return http({
    url: '/getHiddenDanger',
    method: 'get'
  })
}

// 获取帮扶记录
function getHelpRecord() {
  return http({
    url: '/getHelpRecord2',
    method: 'get'
  })
}

// 设备分类及数量
function getEquipment() {
  return http({
    url: '/getEquipment',
    method: 'get'
  })
}

// 隐患分类及数量
function getHiddenDangerClassify() {
  return http({
    url: '/getHiddenDangerClassify',
    method: 'get'
  })
}

export default {
  getHiddenDangerClassify,
  getEquipment,
  getEquipmentLocation,
  getPartyMember,
  getHiddenDanger,
  getHelpRecord
}
