/* eslint-disable no-unused-vars */
import http from '@/utils/http';

/** 查询 仓储物资 集合 */
function getAllStorages(data = {}) {
    return http({
        url: '/materials/list',
        method: 'post',
        data
    });
}

/** 仓储物资 */
function getStorageList(data) {
    return http({
        url: '/materials/page',
        method: 'post',
        data
    });
}
// 新增物资
function addStorageItem(data) {
    return http({
        url: '/materials/create',
        method: 'post',
        data
    });
}
// 修改物资
function updateStorageItem(data) {
    return http({
        url: '/materials/modify',
        method: 'post',
        data
    });
}
// 删除物资
function deleteStorageItem(materialsId) {
    return http({
        url: '/materials/delete',
        method: 'post',
        data: { materialsId }
    });
}
// 新增入库
function handleStockIn(data) {
    return http({
        url: '/warehouse/entry',
        method: 'post',
        data
    });
}
// 新增出库
function handleStockOut(data) {
    return http({
        url: '/warehouse/delivery',
        method: 'post',
        data
    });
}
// 出入库记录
function getStockLog(data) {
    return http({
        url: '/warehouse/page',
        method: 'post',
        data
    });
}
// 撤销出入库
function revokeStockApply(data) {
    const outinModel = data.outinModel;
    delete data.outinModel;
    return http({
        url: ['/warehouse/cancelentry', '/warehouse/canceldelivery'][outinModel],
        method: 'post',
        data
    });
}
/**
 * 获取 农机 分页数据
 * @param { {
 * pageNum: number,
 * pageSize: number,
 * nameplateCode?: string,
 * licensePlateCode?: string,
 * autosteeringSN?: string,
 * tboxSN?: string,
 * name?: string,
 * remark?: number,
 * } } data -
 * pageNum: 页码,
 * pageSize: 每页数量,
 * nameplateCode: 出厂铭牌编号,
 * licensePlateCode: 农机车牌号,
 * autosteeringSN: 搭载丰疆导航套件SN,
 * tboxSN: 搭载丰疆作业监控终端SN,
 * name: 名称,
 * remark: 备注信息,
 * @returns
 * @example getMachinePage({ pageNum：1, pageSize: 10 })
 */
function getMachinePage(data) {
    return http({
        url: `/machine/page`,
        method: 'post',
        data
    });
}

/**
 * @type {{ machineId: string }} machineIdParams
 */
const machineIdParams = { machineId: '' };
/**
 * 删除 指定行 农机 数据
 * @param { machineIdParams } data - machineId: 农机id
 * @returns
 */
function deleteMachine(data) {
    return http({
        url: `/machine/delete`,
        method: 'post',
        data
    });
}

/**
 * 查询详情 指定行 农机 数据
 * @param { machineIdParams } data - machineId: 农机id
 * @returns
 */
function getDetailMachine(data) {
    return http({
        url: `/machine/get`,
        method: 'post',
        data
    });
}

/**
 * 修改 指定行 农机 数据
 * @param { {
 * machineId: string,
 * nameplateCode?: string,
 * licensePlateCode?: string,
 * name?: string,
 * frameCode?: string,
 * engineCode?: string,
 * remark?: string,
 * } } data -
 * machineId: 农机ID,
 * nameplateCode: 出厂铭牌编号,
 * licensePlateCode: 农机车牌号,
 * name: 农机型号,
 * frameCode: 农机架子号,
 * engineCode: 发动机/引擎编号,
 * remark: 备注信息,
 * @returns
 */
function editMachine(data) {
    return http({
        url: `/machine/modify`,
        method: 'post',
        data
    });
}

/**
 * 新增 农机
 * @param { {
 * nameplateCode?: string,
 * licensePlateCode?: string,
 * name?: string,
 * frameCode?: string,
 * engineCode?: string,
 * autosteeringSN?: string,
 * tboxSN?: string,
 * remark?: string,
 * } } data -
 * nameplateCode: 出厂铭牌编号,
 * licensePlateCode: 农机车牌号,
 * name: 农机型号,
 * autosteeringSN: 搭载丰疆导航套件SN,
 * tboxSN: 搭载丰疆作业监控终端SN,
 * frameCode: 农机架子号,
 * engineCode: 发动机/引擎编号,
 * remark: 备注信息,
 * @returns
 */
function addMachine(data) {
    return http({
        url: `/machine/create`,
        method: 'post',
        data
    });
}

/**
 * 农机组合管理
 * @param { {
 * machinePartNameplateCode?: string,
 * autosteeringSN?: string,
 * tboxSN?: string,
 * name: string,
 * machineNameplateCode: string,
 * machineId: string,
 * } } data -
 * machinePartNameplateCode: 出厂铭牌编号,
 * autosteeringSN: 搭载丰疆导航套件SN,
 * tboxSN: 搭载丰疆作业监控终端SN,
 * name: 农机型号,
 * machineNameplateCode: 出厂铭牌编号,
 * machineId: 农机ID,
 * @returns
 */
function updateMachine(data) {
    return http({
        url: `/machine/unit`,
        method: 'post',
        data
    });
}

// 获取全部农机
function getMachineList(data) {
    return http({
        url: `/machine/list`,
        method: 'post',
        data
    });
}

/**
 * 获取 农机型号 分页数据
 * @param { {
 * pageNum: number,
 * pageSize: number,
 * name?: string,
 * brand?: string,
 * remark?: string,
 * beginTime?: string,
 * endTime?: string,
 * machineModel?: number,
 * } } data -
 * pageNum: 页码,
 * pageSize: 每页数量,
 * name: 型号名称,
 * brand: 品牌名称,
 * remark: 备注信息,
 * beginTime: 创建时间范围(开始时间),
 * endTime: 创建时间范围(结束时间),
 * machineModel: 农机主类型（0:拖拉机 1:旋耕机）,
 * @returns
 * @example getMachineModelPage({ pageNum：1, pageSize: 10 })
 */
function getMachineModelPage(data) {
    return http({
        url: `/machinemodel/page`,
        method: 'post',
        data
    });
}
/**
 * 获取 农机具型号 list 选项
 */
function getMachineModelList() {
    return http({
        url: `/machinemodel/list`,
        method: 'post',
        data: {}
    });
}

/**
 * @type {{ machineModelId: string }} machineModelIdParams
 */
const machineModelIdParams = { machineModelId: '' };
/**
 * 删除 指定行 农机型号 数据
 * @param { machineModelIdParams } data - machineModelId: 农机型号id
 * @returns
 */
function deleteMachineModel(data) {
    return http({
        url: `/machinemodel/delete`,
        method: 'post',
        data
    });
}

/**
 * 查询详情 指定行 农机型号 数据
 * @param { machineModelIdParams } data - machineModelId: 农机型号id
 * @returns
 */
function getDetailMachineModel(data) {
    return http({
        url: `/machinemodel/get`,
        method: 'post',
        data
    });
}

/**
 * 修改 指定行 农机型号 数据
 * @param { {
 * machineModelId: string,
 * name?: string,
 * brand?: string,
 * machineModel?: number,
 * kitAutosteeringStatus?: number,
 * integrationStatus?: number,
 * frontWheelbase?: string,
 * frontRearWheelbase?: string,
 * frontSuspensionDistance?: string,
 * rearSuspensionSistance?: string,
 * antennaCenterDistance?: string,
 * antennaRearDistance?: string,
 * antennaHeight?: string,
 * steeringType?: string,
 * turningTadius?: string,
 * machineModelPicList?: Array.<{name:string, url:string}>,
 * remark?: string,
 * } } data -
 * machineModelId: 农机型号ID,
 * brand: 品牌名称,
 * machineModelPicList: 农机图片,
 * remark: 备注信息,
 * @returns
 */
function editMachineModel(data) {
    return http({
        url: `/machinemodel/modify`,
        method: 'post',
        data
    });
}

/**
 * 新增 农机型号
 * @param { {
 * name?: string,
 * brand?: string,
 * machineModel?: number,
 * kitAutosteeringStatus?: number,
 * integrationStatus?: number,
 * frontWheelbase?: string,
 * frontRearWheelbase?: string,
 * frontSuspensionDistance?: string,
 * rearSuspensionSistance?: string,
 * antennaCenterDistance?: string,
 * antennaRearDistance?: string,
 * antennaHeight?: string,
 * steeringType?: string,
 * turningTadius?: string,
 * machineModelPicList?: Array.<{name:string, url:string}>,
 * remark?: string,
 * } } data -
 * name: 农机类型名称,
 * brand: 品牌名称,
 * machineModelPicList: 图片,
 * remark: 备注信息,
 * @returns
 */
function addMachineModel(data) {
    return http({
        url: `/machinemodel/create`,
        method: 'post',
        data
    });
}

/**
 * 获取 农机具 分页数据
 * @param { {
 * pageNum: number,
 * pageSize: number,
 * nameplateCode?: string,
 * identifyCode?: string,
 * machinePartModelName?: string,
 * workType?: string,
 * createTime?: string,
 * remark?: string,
 * } } data -
 * pageNum: 页码,
 * pageSize: 每页数量,
 * nameplateCode: 机具铭牌编号,
 * identifyCode: 机具识别器编码,
 * machinePartModelName: 农机具型号,
 * @returns
 * @example getMachinePartPage({ pageNum：1, pageSize: 10 })
 */
function getMachinePartPage(data) {
    return http({
        url: `/machinepart/page`,
        method: 'post',
        data
    });
}

/**
 * @type {{ machinePartId: string }} machinePartIdParams
 */
const machinePartIdParams = { machinePartId: '' };
/**
 * 删除 指定行 农机具 数据
 * @param { machinePartIdParams } data - machinePartId: 机具ID
 * @returns
 */
function deleteMachinePart(data) {
    return http({
        url: `/machinepart/delete`,
        method: 'post',
        data
    });
}

/**
 * 查询详情 指定行 农机具 数据
 * @param { machinePartIdParams } data - machinePartId: 机具ID
 * @returns
 */
function getDetailMachinePart(data) {
    return http({
        url: `/machinepart/get`,
        method: 'post',
        data
    });
}

/**
 * 查询选项 农机具
 * @returns
 */
function getMachinePartList(data) {
    return http({
        url: `/machinepart/list`,
        method: 'post',
        data
    });
}

/**
 * 修改 指定行 农机具 数据
 * @param { {
 * machinePartId: string,
 * nameplateCode: string,
 * identifyCode: string,
 * machinePartModelName: string,
 * remark?: string,
 * } } data -
 * machinePartId: 机具铭牌编号ID,
 * nameplateCode: 机具铭牌编号,
 * identifyCode: 机具识别器编码,
 * machinePartModelName: 农机具型号,
 * remark: 备注信息,
 * @returns
 */
function editMachinePart(data) {
    return http({
        url: `/machinepart/modify`,
        method: 'post',
        data
    });
}

/**
 * 新增 农机具
 * @param { {
 * nameplateCode: string,
 * identifyCode: string,
 * machinePartModelName: string,
 * remark?: string,
 * } } data -
 * nameplateCode: 机具铭牌编号,
 * identifyCode: 机具识别器编码,
 * machinePartModelName: 农机具型号,
 * remark: 备注信息,
 * @returns
 */
function addMachinePart(data) {
    return http({
        url: `/machinepart/create`,
        method: 'post',
        data
    });
}

/**
 * 获取 农机具型号 分页数据
 * @param { {
 * pageNum: number,
 * pageSize: number,
 * name?: string,
 * brand?: string,
 * remark?: string,
 * createTime?: string,
 * workType?: string,
 * } } data -
 * pageNum: 页码,
 * pageSize: 每页数量,
 * name: 机具型号名称,
 * brand: 机具品牌名称,
 * remark: 备注信息,
 * createTime: 创建时间,
 * workType: 作业类型,
 * @returns
 * @example getMachinePartModelPage({ pageNum：1, pageSize: 10 })
 */
function getMachinePartModelPage(data) {
    return http({
        url: `/machinepartmodel/page`,
        method: 'post',
        data
    });
}

/**
 * 获取 农机具型号 list选项
 */
function getMachinePartModelList() {
    return http({
        url: `/machinepartmodel/list`,
        method: 'post',
        data: {}
    });
}

/**
 * @type {{ machinePartModelId: string }} machinePartModelIdParams
 */
const machinePartModelIdParams = { machinePartModelId: '' };
/**
 * 删除 指定行 农机具型号 数据
 * @param { machinePartModelIdParams } data - machinePartModelId: 机具型号ID
 * @returns
 */
function deleteMachinePartModel(data) {
    return http({
        url: `/machinepartmodel/delete`,
        method: 'post',
        data
    });
}

/**
 * 查询详情 指定行 农机具型号 数据
 * @param { machinePartModelIdParams } data - machinePartModelId: 机具型号ID
 * @returns
 */
function getDetailMachinePartModel(data) {
    return http({
        url: `/machinepartmodel/get`,
        method: 'post',
        data
    });
}

/**
 * 修改 指定行 农机具型号 数据
 * @param { {
 * machinePartModelId: string,
 * machinePartModelName: string,
 * brand?: string,
 * machinePartModelPicList?: Array.<{name:string, url:string}>,
 * workType?: string,
 * workWidth?: string,
 * remark?: string,
 * } } data -
 * machinePartModelId: 机具型号ID,
 * brand: 机具品牌名称,
 * machinePartModelPicList: 机具型号图片,
 * createTime: 创建时间,
 * workType: 作业类型,
 * remark: 备注信息,
 * @returns
 */
function editMachinePartModel(data) {
    return http({
        url: `/machinepartmodel/modify`,
        method: 'post',
        data
    });
}

/**
 * 新增 农机具型号
 * @param { {
 * machinePartModelName: string,
 * brand?: string,
 * machinePartModelPicList?: Array.<{name:string, url:string}>,
 * workType?: string,
 * workWidth?: string,
 * remark?: string,
 * } } data -
 * name: 机具型号名称,
 * brand: 机具品牌名称,
 * machinePartModelPicList: 机具型号图片,
 * createTime: 创建时间,
 * workType: 作业类型,
 * remark: 备注信息,
 * @returns
 */
function addMachinePartModel(data) {
    return http({
        url: `/machinepartmodel/create`,
        method: 'post',
        data
    });
}

/**
 * 获取 丰疆作业监测终端 分页数据
 * @param { {pageNum: number, pageSize:number } } data - pageNum：页码, pageSize 每页数量
 * @returns
 */
function getKittBoxPage(data) {
    return http({
        url: `/kittbox/page`,
        method: 'post',
        data
    });
}
/**
 * 获取 丰疆作业监测终端 list数据
 * @returns
 */
function getKittBoxList() {
    return http({
        url: `/kittbox/list`,
        method: 'post',
        data: {}
    });
}

/**
 * 删除 指定行 丰疆作业监测终端 数据
 * @param { object } data - tboxSn: 终端SN
 * @param { string } data.tboxSn
 * @returns
 */
function deleteKittBox(data) {
    return http({
        url: `/kittbox/delete`,
        method: 'post',
        data
    });
}

/**
 * 查询详情 指定行 丰疆作业监测终端 数据
 * @param { { tboxSn: string } } data - tboxSn: 终端SN
 * @returns
 */
function getDetailKittBox(data) {
    return http({
        url: `/kittbox/get`,
        method: 'post',
        data
    });
}

/**
 * 修改 指定行 丰疆作业监测终端 数据
 * @param { {
 * tboxSn: string,
 * cardNumber: string,
 * remark?: string,
 * } } data - tboxSn: 终端SN, cardNumber: 物联网卡号, remark: 备注
 * @returns
 */
function editKittBox(data) {
    return http({
        url: `/kittbox/modify`,
        method: 'post',
        data
    });
}

/**
 * 新增 丰疆作业监测终端
 * @param { {
 * tboxSn: string,
 * cardNumber: string,
 * remark?: string,
 * } } data -
 * tboxSn: 终端SN, cardNumber: 物联网卡号, remark: 备注,
 * @returns
 */
function addKittBox(data) {
    return http({
        url: `/kittbox/create`,
        method: 'post',
        data
    });
}

/**
 * 获取 导航套件 分页数据
 * @param { {pageNum: number, pageSize:number } } data - pageNum：页码, pageSize 每页数量
 * @returns
 */
function getKitAutosteeringPage(data) {
    return http({
        url: `/kitautosteering/page`,
        method: 'post',
        data
    });
}
/**
 * 获取 导航套件 list
 */
function getKitAutosteeringList(data) {
    return http({
        url: `/kitautosteering/list`,
        method: 'post',
        data
    });
}

/**
 * 删除 指定行 导航套件 数据
 * @param { { autoSteeringSn: string } } data - autoSteeringSn: 套件SN
 * @returns
 */
function deleteKitAutosteering(data) {
    return http({
        url: `/kitautosteering/delete`,
        method: 'post',
        data
    });
}

/**
 * 查询详情 指定行 导航套件 数据
 * @param { { autoSteeringSn: string } } data - autoSteeringSn: 套件SN
 * @returns
 */
function getDetailKitAutosteering(data) {
    return http({
        url: `/kitautosteering/get`,
        method: 'post',
        data
    });
}

/**
 * 查询 导航套件 搭载的机车数据
 * @param { { autoSteeringSn: string } } data - autoSteeringSn: 套件SN
 * @returns
 */
function getMachiInfoKitAutosteering(data) {
    return http({
        url: `/kitautosteering/getvehicleinfo`,
        method: 'post',
        data
    });
}
/**
 * 导航套件 解锁/锁机
 * @param { { autoSteeringSn: string, type: number } } data - autoSteeringSn: 套件SN; type,执行类型，1 解锁，2 锁机
 * @returns
 */
//  被盗解锁 unLock
function lockKitAutosteering({ autoSteeringSn, type = '1' }) {
    let lastTargetUrl = '';
    switch (type) {
        case '0':
            lastTargetUrl = 'lock';
            break;
        default:
            lastTargetUrl = 'unlock';
            break;
    }
    return http({
        url: `/kitautosteering/${lastTargetUrl}`,
        method: 'post',
        data: { autoSteeringSn }
    });
}

/**
 * 修改 指定行 导航套件 数据
 * @param { {
 * autoSteeringSn: string,
 * cardNumber: string,
 * remark?: string,
 * } } data - { autoSteeringSn: 终端SN, cardNumber: 物联网卡号, remark: 备注 }
 * @returns
 */
function editKitAutosteering(data) {
    return http({
        url: `/kitautosteering/modify`,
        method: 'post',
        data
    });
}

/**
 * 新增 导航套件
 * @param { {
 * autoSteeringSn: string,
 * cardNumber: string,
 * remark?: string,
 * } } data - { autoSteeringSn: 终端SN, cardNumber: 物联网卡号, remark: 备注 }
 * @returns
 */
function addKitAutosteering(data) {
    return http({
        url: `/kitautosteering/create`,
        method: 'post',
        data
    });
}

// 收割机 设备管理
function getKitIntegratedPage(data) {
    return http({
        url: `/kitintegrated/page`,
        method: 'post',
        data
    });
}

// 作业批次
function getKitIntegratedBatch(data) {
    return http({
        url: `/kitintegrated/batch`,
        method: 'post',
        data
    });
}

// 作业详情
function getKitIntegratedHistory(data) {
    return http({
        url: `/kitintegrated/workhistory`,
        method: 'post',
        data
    });
}

// 历史数据
function getKitIntegratedHistorySum(id) {
    return http({
        url: `/kitintegrated/workhistorysum`,
        method: 'post',
        data: { id: Number(id), noLoading: true }
    });
}

/**
 * 修改 指定行 设备管理 割草机 数据
 * @param { {
 * autoSteeringSn: string,
 * cardNumber: string,
 * remark?: string,
 * } } data - { autoSteeringSn: 终端SN, cardNumber: 物联网卡号, remark: 备注 }
 * @returns
 */
function editKitintegrated(data) {
    return http({
        url: `/kitintegrated/modify`,
        method: 'post',
        data
    });
}

/**
 * 新增 设备管理 割草机
 * @param { {
 * autoSteeringSn: string,
 * cardNumber: string,
 * remark?: string,
 * } } data - { autoSteeringSn: 终端SN, cardNumber: 物联网卡号, remark: 备注 }
 * @returns
 */
function addKitintegrated(data) {
    return http({
        url: `/kitintegrated/create`,
        method: 'post',
        data
    });
}

/**
 * 删除 指定行 设备管理 割草机 数据
 * @param { { autoSteeringSn: string } } data - autoSteeringSn: 套件SN
 * @returns
 */
function deleKekitintegrated(data) {
    return http({
        url: `/kitintegrated/delete`,
        method: 'post',
        data
    });
}

/**
 * 查询详情 指定行 设备管理 割草机 数据
 * @param { { autoSteeringSn: string } } data - autoSteeringSn: 套件SN
 * @returns
 */
function getDetailKitintegrated(data) {
    return http({
        url: `/kitintegrated/get`,
        method: 'post',
        data
    });
}

export default {
    getKitIntegratedBatch,
    getKitIntegratedHistorySum,
    getKitIntegratedHistory,
    editKitintegrated,
    addKitintegrated,
    deleKekitintegrated,
    getDetailKitintegrated,
    getAllStorages,
    getStorageList,
    addStorageItem,
    updateStorageItem,
    deleteStorageItem,
    handleStockIn,
    handleStockOut,
    getStockLog,
    revokeStockApply,
    getMachinePage,
    deleteMachine,
    getDetailMachine,
    editMachine,
    addMachine,
    updateMachine,
    getMachineList,
    getMachineModelPage,
    getMachineModelList,
    deleteMachineModel,
    getDetailMachineModel,
    editMachineModel,
    addMachineModel,
    getMachinePartModelPage,
    getMachinePartModelList,
    deleteMachinePartModel,
    getDetailMachinePartModel,
    editMachinePartModel,
    addMachinePartModel,
    getMachinePartPage,
    deleteMachinePart,
    getDetailMachinePart,
    getMachinePartList,
    editMachinePart,
    addMachinePart,
    getKittBoxPage,
    deleteKittBox,
    getDetailKittBox,
    editKittBox,
    addKittBox,
    getKitAutosteeringPage,
    deleteKitAutosteering,
    getKitAutosteeringList,
    getDetailKitAutosteering,
    getMachiInfoKitAutosteering,
    editKitAutosteering,
    getKittBoxList,
    lockKitAutosteering,
    addKitAutosteering,
    getKitIntegratedPage
};