import http from '@/utils/http'

/* 农场管理 */
// 获取农场列表
function getFarmPage(data) {
  return http({
    url: '/farm/page',
    method: 'post',
    data
  })
}

function getFarmList(data) {
  return http({
    url: '/farm/list',
    method: 'post',
    data
  })
}

// 新增农场
function addFarm(data) {
  return http({
    url: '/farm/create',
    method: 'post',
    data
  })
}

// 修改农场
function updateFarm(data) {
  return http({
    url: '/farm/modify',
    method: 'post',
    data
  })
}

// 删除农场
function deleteFarm(data) {
  return http({
    url: '/farm/delete',
    method: 'post',
    data
  })
}

// 查看农场
function getFarm(data) {
  return http({
    url: '/farm/get',
    method: 'post',
    data
  })
}

/* 地块管理 */

// 地块列表
function getFarmLandPage(data) {
  return http({
    url: '/farmland/page',
    method: 'post',
    data
  })
}

// 新增地块
function addFarmLand(data) {
  return http({
    url: '/farmland/create',
    method: 'post',
    data
  })
}

// 修改地块前查询
function beforeUpdateFarmLand(data) {
  return http({
    url: '/farmland/beforemodify',
    method: 'post',
    data
  })
}

// 修改地块
function updateFarmLand(data) {
  return http({
    url: '/farmland/modify',
    method: 'post',
    data
  })
}

// 删除地块
function deleteFarmLand(data) {
  return http({
    url: '/farmland/delete',
    method: 'post',
    data
  })
}

// 查看地块
function getFarmland(data) {
  return http({
    url: '/farmland/get',
    method: 'post',
    data
  })
}

// 查询地块集合
function getAllFarmland(data) {
  return http({
    url: '/farmland/list',
    method: 'post',
    data
  })
}

// 查询田块类型的地块集合
function getFarmlandByType(data) {
  return http({
    url: '/farmland/listbytype',
    method: 'post',
    data
  })
}

/* 地块分区方案管理 */

// 分区列表
function getFarmLandPartPage(data) {
  return http({
    url: '/farmlandpartition/page',
    method: 'post',
    data
  })
}

// 分区集合
function getFarmLandPartList(data) {
  return http({
    url: '/farmlandpartition/list',
    method: 'post',
    data
  })
}

// 新增分区
function addFarmLandPart(data) {
  return http({
    url: '/farmlandpartition/create',
    method: 'post',
    data
  })
}
// 删除分区
function deleteFarmLandPart(data) {
  return http({
    url: '/farmlandpartition/delete',
    method: 'post',
    data
  })
}

// 查看分区
function getFarmlandPart(data) {
  return http({
    url: '/farmlandpartition/get',
    method: 'post',
    data
  })
}

/* 地块租赁管理 */

// 列表
function getFarmLandTransfPage(data) {
  return http({
    url: '/farmlandtransfer/page',
    method: 'post',
    data
  })
}

// 新增
function addFarmLandTransf(data) {
  return http({
    url: '/farmlandtransfer/create',
    method: 'post',
    data
  })
}
// 修改
function updateFarmLandTransf(data) {
  return http({
    url: '/farmlandtransfer/modify',
    method: 'post',
    data
  })
}
// 删除
function deleteFarmLandTransf(data) {
  return http({
    url: '/farmlandtransfer/delete',
    method: 'post',
    data
  })
}

// 终止
function stopFarmLandTransf(data) {
  return http({
    url: 'farmlandtransfer/terminate',
    method: 'post',
    data
  })
}

// 查看
function getFarmlandTransf(data) {
  return http({
    url: '/farmlandtransfer/select',
    method: 'post',
    data
  })
}

export default {
  getFarmPage,
  getFarmList,
  addFarm,
  updateFarm,
  deleteFarm,
  getFarm,
  getFarmLandPage,
  addFarmLand,
  beforeUpdateFarmLand,
  updateFarmLand,
  deleteFarmLand,
  getFarmland,
  getAllFarmland,
  getFarmlandByType,
  getFarmLandPartPage,
  getFarmLandPartList,
  addFarmLandPart,
  deleteFarmLandPart,
  getFarmlandPart,
  getFarmLandTransfPage,
  addFarmLandTransf,
  updateFarmLandTransf,
  deleteFarmLandTransf,
  stopFarmLandTransf,
  getFarmlandTransf
}
