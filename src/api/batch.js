import http from '@/utils/http'

/* 种植批次管理 */
// 获取批次列表
function getBatchList(data) {
  return http({
    url: '/farmingbatch/page',
    method: 'post',
    data
  })
}
function getBatchListForSelect(data = {}) {
  return http({
    url: '/farmingbatch/list',
    method: 'post',
    data
  })
}
// 获取批次详情
function getBatchInfo(params) {
  return http({
    url: '/farmingbatch/get',
    method: 'post',
    data: params
  })
}
// 新增批次
function addBatch(data) {
  return http({
    url: '/farmingbatch/create',
    method: 'post',
    data
  })
}
// 修改批次
function updateBatch(data) {
  return http({
    url: '/farmingbatch/modify',
    method: 'post',
    data
  })
}
// 删除批次
function deleteBatch(data) {
  return http({
    url: '/farmingbatch/delete',
    method: 'post',
    data
  })
}
// 查询批次中的地块
function getBatchFields(data) {
  return http({
    url: '/farmingbatch/farmingbatchfarmlandrel/get',
    method: 'post',
    data
  })
}
// 修改地块进度
function updateFieldProgress(data) {
  return http({
    url: '/farmingbatch/modifypart',
    method: 'post',
    data
  })
}
// 修改批次进度
function updateBatchProgress(data) {
  return http({
    url: '/farmingbatch/modifyall',
    method: 'post',
    data
  })
}
// 结束批次
function finishBatch(data) {
  return http({
    url: '/farmingbatch/finish',
    method: 'post',
    data
  })
}
// 查询地块使用状态
function getFarmlandStatus(data) {
  return http({
    url: '/farmingbatch/usefarmland',
    method: 'post',
    data: data
  })
}

/** 字典表 */
function getDictData(data) {
  return http({
    url: 'dict/get',
    method: 'post',
    data
  })
}

export default {
  getBatchList,
  getBatchListForSelect,
  getBatchInfo,
  addBatch,
  updateBatch,
  deleteBatch,
  getBatchFields,
  updateFieldProgress,
  updateBatchProgress,
  finishBatch,
  getFarmlandStatus,
  getDictData
}
