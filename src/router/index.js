import Vue from 'vue'
import VueRouter from 'vue-router'
import Layout from '@/views/Dashboard'

Vue.use(VueRouter)

export const constantRoutes = [
  {
    path: '/',
    component: Layout
  }
]

const router = new VueRouter({
  mode: 'hash',
  routes: constantRoutes
})

export default router
