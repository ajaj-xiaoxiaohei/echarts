export default {
  data () {
    return {
      autoInit: true,
      pageDesc: {
        pageNum: 1
      },
      totalCount: 0,
      tableData: []
    }
  },
  created () {
    if (this.autoInit) this.getTableData()
  },
  methods: {
    getTableData (pageNum = 1) {
      if (!this.request) {
        throw Error('Request Null')
      }
      this.pageDesc.pageNum = pageNum
      this.pageDesc.pageSize = 10
      let params = Object.assign({}, this.pageDesc, this.queryParams || {})
      for (let key in params) {
        if (params[key] === '') delete params[key]
      }
      this.request(params).then(resp => {
        // 删除当前页面最后一条数据，跳转前一页
        if (resp.data && resp.data.length === 0 && pageNum > 1) {
          this.getTableData(--pageNum)
          return
        }
        this.tableData = resp.data || []
        this.totalCount = resp.totalCount
      })
    },
    refreshCurPage () {
      this.getTableData(this.pageDesc.pageNum)
    },
    handleSizeChange (pageSize) {
      this.pageDesc.pageSize = pageSize
      this.getTableData()
    },
    resetQueryParams () {
      if (this.queryParams) {
        this.queryParams = this.$options.data().queryParams
      }
      this.pageDesc.pageNum = 1
      this.getTableData()
    }
  }
}
