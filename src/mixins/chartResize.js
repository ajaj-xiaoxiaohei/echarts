
import * as echarts from 'echarts/core'
import { debounce } from '@/utils/utils'
export default {
  data () {
    return {
      chartArr: []
    }
  },
  methods: {
    initChart (dom) {
      let chart = echarts.init(dom)
      this.chartArr.push(chart)
      return chart
    },
    chartResize () {
      debounce(() => {
        let chartArr = this.chartArr
        for (let i = 0; i < chartArr.length; i++) {
          // 此处定时器是为了在页面存在多个图时，resize方法的调用时间微微错开，避免明显的卡顿
          setTimeout(function () {
            chartArr[i].resize()
          }, 100)
        }
      }, 500)
    }
  },

  mounted () {
    window.addEventListener('resize', this.chartResize)
  },

  beforeDestroy () {
    window.removeEventListener('resize', this.chartResize)
    let chartArr = this.chartArr
    for (let i = 0; i < chartArr.length; i++) {
      echarts.dispose(chartArr[i])
    }
    this.chartArr = []
  }
}
