import router from '@/router/index'
import API from '@/api/index'

export default {
  namespaced: true,
  state: {
    userId: localStorage.getItem('userId') ? Number(localStorage.getItem('userId')) : '',
    username: localStorage.getItem('username') || '',
    nickname: localStorage.getItem('nickname') || '',
    orgName: localStorage.getItem('orgName') || '',
    permissions: localStorage.getItem('permissions') ? JSON.parse(localStorage.getItem('permissions')) : [],
    userMenus: undefined
  },
  mutations: {
    SET_USERID (state, userId = '') {
      localStorage.setItem('userId', userId)
      state.userId = userId
    },
    SET_NICKNAME (state, nickname = '') {
      localStorage.setItem('nickname', nickname)
      state.nickname = nickname
    },
    SET_USERNAME (state, username = '') {
      localStorage.setItem('username', username)
      state.username = username
    },
    SET_ORGNAME (state, orgName = '') {
      localStorage.setItem('orgName', orgName)
      state.orgName = orgName
    },
    SET_PERMISSIONS (state, permissions = []) {
      localStorage.setItem('permissions', JSON.stringify(permissions))
      state.permissions = permissions
    },
    SET_MENUS (state, userMenus = undefined) {
      state.userMenus = userMenus
    }
  },
  actions: {
    login ({ commit }, data) {
      return API.user.login(data).then(resp => {
        localStorage.setItem('TOKEN', resp.data.Authorization)
        commit('SET_PERMISSIONS', resp.data.permissions)
        commit('SET_USERID', resp.data.userId)
        commit('SET_USERNAME', resp.data.userName)
        commit('SET_NICKNAME', resp.data.nickName)
        commit('SET_ORGNAME', resp.data.orgName)
        // router.replace('/farmland/index')
        return resp
      })
    },
    logout ({ commit }) {
      localStorage.removeItem('TOKEN')
      localStorage.removeItem('permissions')
      localStorage.removeItem('userId')
      localStorage.removeItem('username')
      localStorage.removeItem('nickname')
      location.reload()
      // router.replace(`/${PARTY}/login`)
    },
    getUserMenus ({ commit }) {
      return API.user.getUserMenus().then(resp => {
        const asyncRoutes = generateRoutes(resp.data)
        const viewList = asyncRoutes.filter(i => !i.hidden)
        const routes = [...asyncRoutes]
        if (viewList.length) {
          routes.push({
            path: '/*',
            redirect: viewList[0].path + '/' + viewList[0].children[0].path
          })
        }
        console.log(router)
        router.addRoutes(routes)

        commit('SET_MENUS', asyncRoutes)
      })
    }
  }
}

function generateRoutes (list = []) {
  list = list.filter(i => i.component).map(item => {
    let _item = {
      id: item.id,
      parentId: item.parentId,
      path: item.path || '',
      component: resolve => require([`@/views/${item.component}`], resolve),
      hidden: item.visible === 1,
      name: firstWordUpperCase(item.path),
      meta: {
        title: item.menuName,
        icon: item.icon
      },
      orderNum: item.orderNum
    }
    if (_item.parentId === 0) {
      _item.path = '/' + _item.path
    }
    return _item
  })
  // 循环所有项
  const treeData = list.filter(father => {
    let branchArr = list.filter(child => {
      // 返回每一项的子级数组
      return father['id'] === child['parentId']
    })
    if (branchArr.length > 0) {
      branchArr.sort((a, b) => {
        return a.orderNum - b.orderNum
      })
      father.children = branchArr
      const temp = branchArr.filter(i => !i.hidden)
      if (temp.length) {
        father.redirect = father.path + '/' + temp[0].path
      }
    }
    // 返回第一层
    return father['parentId'] === 0
  })
  treeData.sort((a, b) => {
    return a.orderNum - b.orderNum
  })
  return treeData !== '' ? treeData : []
}
/**
 * 字符串首字母大写
 * @param {*} str
 */
function firstWordUpperCase (str) {
  return str.toLowerCase().replace(/(\s|^)[a-z]/g, function (char) {
    return char.toUpperCase()
  })
}
