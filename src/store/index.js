import Vue from 'vue'
import Vuex from 'vuex'

import user from './user'
import en from '@/i18n/en';
import zh from '@/i18n/zh';
import ja from '@/i18n/ja';
import { getLanguage } from '@/utils/utils'

Vue.use(Vuex)
const languageList = [
  { key: 'zh', label: '简体中文', title: zh.shortTitle + zh.title },
  { key: 'en', label: 'English', title: en.shortTitle },
  { key: 'ja', label: '日本語', title: zh.shortTitle + ja.title }
]

export default new Vuex.Store({
  modules: { user },
  state: {
    lang: getLanguage(),
    title: ''
  },
  getters: {
    langTxt(state) {
      const langItem = languageList.find(i => i.key === state.lang)
      if (langItem?.title) {
        document.title = langItem.title
      }
      return langItem?.label
    },
    languageList(state) {
      return (languageList.filter(i => i.key !== state.lang))
    }
  },
  mutations: {
    setLang(state, lang = 'en') {
      location.reload();
      state.lang = lang
      localStorage.setItem('lang', lang)
    },
    setTitle(state, title) {
      state.title = title
    }
  },
  actions: {
  }
})
