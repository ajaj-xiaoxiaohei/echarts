
import router from './router'
import store from './store'

router.beforeEach((to, from, next) => {
  console.log(to)
  const TOKEN = localStorage.getItem('TOKEN')
  const PARTY = localStorage.getItem('PARTY') || 'farmbook'
  if (to.meta.noAuth) {
    next()
  } else if (to.path.endsWith('/login') && TOKEN) { // 有Token不再跳转登录
    next(from.path)
  } else if (!to.path.endsWith('/login')) {
    if (!TOKEN) { // 无Token转登录
      next(`/${PARTY}/login`)
    } else if (store.state.user.userMenus === undefined) {
      store.dispatch('user/getUserMenus').then(() => {
        next({ ...to, replace: true })
      })
    } else {
      next()
    }
  } else {
    next()
  }
})
