import Vue from 'vue'
import cfmbox from './ConfirmBox'

const DefaultOpts = {
  // cancelButtonText: '取消',
  // confirmButtonText: '确定',
  // showCustom: false,
  // customButtonText: '按钮'
}
let currentMsg, instance
const ConfirmBoxConstructor = Vue.extend(cfmbox)

const ConfirmBox = function (message, options = {}) {
  if (!instance) {
    instance = new ConfirmBoxConstructor({
      el: document.createElement('div')
    })
    instance.callback = () => {
      if (currentMsg) {
        if (instance.action === 'cancel') {
          currentMsg.reject(instance.action)
        } else {
          currentMsg.resolve(instance.action)
        }
      }
    }
  }
  options = Object.assign({}, DefaultOpts, options)
  for (const key in options) {
    instance[key] = options[key]
  }
  instance.message = message

  document.body.appendChild(instance.$el)
  Vue.nextTick(() => {
    instance.visible = true
  })

  return new Promise((resolve, reject) => {
    currentMsg = { resolve, reject }
  })
}

Vue.prototype.$confirmbox = ConfirmBox
