// axios组件初始化
import axios from 'axios'
import { Message } from 'element-ui'
import { openLoading, closeLoading, getLanguage } from './utils'
import store from '../store'
import qs from 'qs'

// 相同的提示在一定时间内提示一遍
// let msgIns = null
let msgArr = []
function msgClose(ins) {
  // msgIns = null
  const index = msgArr.find(msg => msg === ins.message)
  msgArr.splice(index, 1)
}

const langHead = {
  zh: 'zh-CN',
  en: 'en-US',
  ja: 'ja-JP'
}

const lang = getLanguage()

const service = axios.create({
  baseURL: process.env.VUE_APP_BASE_API, // api的base_url
  withCredentials: true, // send cookies when cross-domain requests
  headers: {
    'x-platform-header': 'saas-farm',
    'accept-language': langHead[lang] || langHead.zh
  },
  timeout: 70000 // 请求超时时间
})

// http request 拦截器
service.interceptors.request.use(
  config => {
    // config.headers.common['X-Access-Token'] = localStorage.getItem('TOKEN') || 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2NTYxNDM4MDIsInVzZXJuYW1lIjoiYWRtaW4ifQ.wjxRoAbf1EKdIoA0izdkEl0ASJLMU9Xick1QsnBkixk'
    if (config.data) {
      if (config.data.noLoading) {
        delete config.data.noLoading
        config.noLoading = true
      }
      if (config.data.needStringify) {
        delete config.data.needStringify
        config.data = qs(config.data)
      }
      // for (let key in config.data) {
      //   if (config.data[key] === '' || config.data[key] === undefined) {
      //     delete config.data[key]
      //   }
      // }
    } else if (config.params) {
      if (config.params.noLoading) {
        delete config.params.noLoading
        config.noLoading = true
      }
      // for (let key in config.params) {
      //   if (config.params[key] === '' || config.params[key] === undefined) {
      //     delete config.params[key]
      //   }
      // }
    }
    if (!config.noLoading) openLoading() // 默认开启Loading
    return config
  },
  error => {
    return Promise.reject(error)
  }
)

// http response 拦截器
service.interceptors.response.use(
  response => {
    if (!response.config.noLoading) {
      closeLoading()
    }
    if (response.data instanceof Blob) {
      return Promise.resolve(response.data)
    } else if (response.data.code !== 200) {
      let rejection = {
        config: response.config,
        status: response.data.resultCode || response.data.code,
        message: response.data.resultMsg || response.data.msg
      }
      const msg = rejection.message
      if (!msgArr.includes(msg)) {
        msgArr.push(msg)
        Message.error({ message: msg, onClose: msgClose })
      }
      // if (!msgIns) msgIns = Message.error({ message: msg, onClose: msgClose })
      return Promise.reject(rejection)
    }
    return Promise.resolve(response.data)
  },
  error => {
    if (!error.config.noLoading) {
      closeLoading()
    }
    if (error.response) {
      if (error.response.status === 403 || error.response.status === 401) {
        // store.dispatch('user/logout')
        console.log(store)
      } else if (error.response.data && error.response.data instanceof Blob) {
        let reader = new FileReader()
        reader.readAsText(error.response.data, 'utf-8')
        reader.onload = function (e) {
          let res = JSON.parse(reader.result)
          const msg = res.resultMsg || res.msg
          if (!msgArr.includes(msg)) {
            msgArr.push(msg)
            Message.error({ showClose: true, message: msg, onClose: msgClose })
          }
          // if (!msgIns) msgIns = Message.error({ message: res.resultMsg, onClose: msgClose })
        }
      } else {
        if (error.response.data && (error.response.data.resultMsg || error.response.data.msg)) {
          const msg = error.response.data.resultMsg || error.response.data.msg
          if (!msgArr.includes(msg)) {
            msgArr.push(msg)
            Message.error({ showClose: true, message: msg, onClose: msgClose })
          }
          // if (!msgIns) msgIns = Message.error({ message: msg, onClose: msgClose })
        } else {
          const msg = error.response.status + ' ' + error.response.statusText
          if (!msgArr.includes(msg)) {
            msgArr.push(msg)
            Message.error({ showClose: true, message: msg, onClose: msgClose })
          }
          // if (!msgIns) msgIns = Message.error({ message: msg, onClose: msgClose })
        }
      }
    } else if (error.message) {
      const msg = error.message
      if (!msgArr.includes(msg)) {
        msgArr.push(msg)
        Message.error({ message: msg, onClose: msgClose })
      }
      // if (!msgIns) msgIns = Message.error({ message: msg, onClose: msgClose })
    }
    return Promise.reject(error)
  }
)

export default service
