(function (doc, win) {
  let docEl = doc.documentElement
  let resizeEvt = 'orientationchange' in window ? 'orientationchange' : 'resize'
  let timer = null

  let recalc = function () {
    window.rate = 1
    const clientWidth = docEl.clientWidth
    const clientHeight = docEl.clientHeight
    if (!clientWidth || !clientHeight) return
    window.rate = Math.min(Math.max(clientWidth, 1200) / 1920, Math.max(clientHeight, 678) / 1080)
    docEl.style.fontSize = 100 * window.rate + 'px'
  }
  recalc()

  if (!doc.addEventListener) return
  // 防抖
  win.addEventListener(resizeEvt, () => {
    if (timer) clearTimeout(timer)
    timer = setTimeout(recalc, 300)
  }, false)
})(document, window)
