import { Loading } from 'element-ui'
import COS from 'cos-js-sdk-v5'

const required = () => { throw new Error('Missing parameter') }

// 全屏loading弹层
let loadingInstance = null
let loadCount = 0
function openLoading () {
  if (loadCount === 0) {
    loadingInstance = Loading.service({
      lock: true,
      background: 'rgba(255, 255, 255, 0.37)'
    })
  }
  loadCount++
}
function closeLoading () {
  loadCount--
  if (loadCount === 0) {
    loadCount = 0
    loadingInstance && loadingInstance.close()
  }
}

function getToken () {
  return localStorage.getItem('TOKEN')
}

function setToken (token = required()) {
  return localStorage.setItem('TOKEN', token)
}

function removeToken () {
  localStorage.removeItem('TOKEN')
}

function downloadFile (file, fileName = new Date().getTime()) {
  // application/vnd.openxmlformats-officedocument.spreadsheetml.sheet
  let url = window.URL.createObjectURL(file)
  let link = document.createElement('a')
  link.href = url
  link.style.display = 'none'
  link.setAttribute('download', fileName) // 自定义文件名，Firefox/Chrome支持
  link.click()
}

/**
 * 构造树型结构数据
 * @param {*} data 数据源
 * @param {*} id id字段 默认 'id'
 * @param {*} parentId 父节点字段 默认 'parentId'
 * @param {*} children 孩子节点字段 默认 'children'
 * @param {*} rootId 根Id 默认 0
 */
function handleTree (data, id, parentId, children, rootId) {
  id = id || 'id'
  parentId = parentId || 'parentId'
  children = children || 'children'
  rootId = rootId || Math.min.apply(Math, data.map(item => { return item[parentId] })) || 0
  // 对源数据深度克隆
  const cloneData = JSON.parse(JSON.stringify(data))
  // 循环所有项
  const treeData = cloneData.filter(father => {
    let branchArr = cloneData.filter(child => {
      // 返回每一项的子级数组
      return father[id] === child[parentId]
    })
    if (branchArr.length > 0) {
      branchArr.sort((a, b) => {
        return a.orderNum - b.orderNum
      })
      father.children = branchArr
    } else {
      father.children = undefined
    }
    // 返回第一层
    return father[parentId] === rootId
  })
  treeData.sort((a, b) => {
    return a.orderNum - b.orderNum
  })
  return treeData !== '' ? treeData : data
}

function m2tomu (num = 0) {
  const rate = 666.6666667
  return Number((num / rate).toFixed(2))
}

let timeout = null
function debounce (fn, wait) {
  if (timeout !== null) clearTimeout(timeout)
  timeout = setTimeout(fn, wait)
}

/**
 * 面积转换
 * @param {*} area 面积
 * @param {*} fromUnit 传入的单位
 * @param {*} toUnit 需要成转换的单位
 * @param {*} 默认m2 不转换  默认保留2位小数
 */
function transfArea (area = 0, fromUnit = 'm2', toUnit = 'm2') {
  if (fromUnit === toUnit) return Math.round(area * 100000000) / 100000000
  if (fromUnit === 'm2' && toUnit === 'mu') {
    return Math.round(area / 666.66666667 * 100000000) / 100000000
  } else if (fromUnit === 'mu' && toUnit === 'm2') {
    return Math.round(area * 666.66666667 * 100000000) / 100000000
  }
}

/**
 * 获取国际化设置
 * @returns {string} 国际化标识
 */
function getLanguage() {
  let lang
  const localStorageLang = localStorage.getItem('lang')
  if (!localStorageLang) {
    const browserLanguage = (navigator.language || navigator.browserLanguage).toLowerCase()
    switch (browserLanguage) {
      case 'zh-cn':
      case 'zh':
        lang = 'zh'
        break;
      case 'ja':
        lang = 'ja'
        break;
      default:
        lang = 'en'
        break;
    }
  } else {
    lang = localStorageLang
  }
  return lang
}

export {
  openLoading,
  closeLoading,
  getToken,
  setToken,
  removeToken,
  downloadFile,
  handleTree,
  m2tomu,
  debounce,
  transfArea,
  getLanguage
}

const base = {
  install (Vue, options) {
    Vue.prototype._handleTree = handleTree
    Vue.prototype._downloadFile = downloadFile
    Vue.prototype._transfArea = transfArea
    Vue.prototype._COSClient = new COS({ SecretId: 'AKIDqVu3wYtxkHXWFhsmPmshGYZJmyuZx5wW', SecretKey: 'LQEm32kCOaVAYfU2KL5XidGchV2IH0Sd' })
    Vue.prototype._COSOpts = { Bucket: 'fj-agric-saas-01-1304943718', Region: 'ap-beijing', StorageClass: 'STANDARD' }
  }
}

export default base
