// import API from '@/api';
import { mapState } from 'vuex';
export default {
  data() {
    return {
      unitConfig: {
        // 1 公制单位 2 英制单位 3 自定义单位
        metrologyType: '',
        // 1 公里/km,米/m,厘米/cm 2 英里/mile,英尺/ft,英寸/inch
        lengthUnit: '',
        // 1 千米每小时 km 2 米每秒 m/s  3 英里每小时 mph
        speedUnit: '',
        // 1 公顷/ha 2 英亩/acre 3 亩/mu
        outAreaUnit: '',
        // 1 平方米/m² 2 平方英尺/ft²
        inAreaUnit: ''
      }
    };
  },
  computed: {
    ...mapState(['lang']),
    showOutAreaUnitTxt() {
      let txt = this.$t('txt.mu');
      switch (this.unitConfig.outAreaUnit) {
        case '1':
          txt = this.$t('txt.ha');
          break;
        case '2':
          txt = this.$t('txt.acre');
          break;
      }
      return txt;
    },
    showInAreaUnit() {
      let txt = 'm²';
      switch (this.unitConfig.inAreaUnit) {
        case '2':
          txt = 'ft²';
          break;
      }
      return txt;
    },
    showSpeedUnit() {
      let resUnit;
      switch (this.unitConfig.speedUnit) {
        case '2':
          resUnit = 'm/s';
          break;
        case '3':
          resUnit = 'mp/h';
          break;
        default:
          resUnit = 'km/h';
          break;
      }
      return resUnit;
    }
  },
  created() {
    // API.home.metrologyGet().then((resp) => {
    //   // 默认为自定义，并且单位都是公制
    //   let {
    //     metrologyType = '3',
    //     lengthUnit = '1',
    //     speedUnit = '1',
    //     outAreaUnit = '1',
    //     inAreaUnit = '1'
    //   } = resp.data || {};
    //   //  语言环境为中文时，且为自定义设置单位时, 则默认为亩
    //   if (this.lang === 'zh' && !resp?.data?.outAreaUnit) {
    //     outAreaUnit = '3';
    //   }
    //   switch (metrologyType) {
    //     case '1':
    //       lengthUnit = '1';
    //       speedUnit = '1';
    //       outAreaUnit = '1';
    //       inAreaUnit = '1';
    //       break;
    //     case '2':
    //       lengthUnit = '2';
    //       speedUnit = '3';
    //       outAreaUnit = '2';
    //       inAreaUnit = '2';
    //       break;
    //     default:
    //       if (!lengthUnit) lengthUnit = '1';
    //       if (!speedUnit) speedUnit = '1';
    //       if (!outAreaUnit) outAreaUnit = '1';
    //       if (!inAreaUnit) inAreaUnit = '1';
    //       break;
    //   }
    //   this.unitConfig = {
    //     metrologyType,
    //     lengthUnit,
    //     speedUnit,
    //     outAreaUnit,
    //     inAreaUnit
    //   };
    // });
  },
  methods: {
    /**
     * 面积转换
     * @param {*} area 面积
     * @param {*} fromUnit 传入的单位 默认m2 可以为mu，m2
     * @param {*} isReverse 是否逆向转化 默认否
     * @param {*} isInArea 是否是室内面积，默认否
     * @param {*} validator 约束，可以填一个数字区域 [min, max]
     * @param {*} fixed 正转化约束小数位
     */
    unitTransfArea(
      area = 0,
      fromUnit = 'mu',
      isInArea = false,
      isReverse = false,
      validator = [],
      fixed = 2
    ) {
      // 公顷系数
      const HA = 10000;
      // 英亩系数
      const ACRE = 4046.8564;
      // 亩系数
      const MU = 666.6667;
      // 平方英尺系数
      const FT2 = 0.0929;
      const coefficient = {
        // 平方米 转 公顷
        m2to1: 1 / HA,
        // 平方米 转 英亩
        m2to2: 1 / ACRE,
        // 平方米 转 亩
        m2to3: 1 / MU,
        // 亩 转 公顷
        muto1: MU / HA,
        // 亩 转 英亩
        muto2: MU / ACRE,
        // 室内面积 平方米 转 平方英尺
        m2to2in: 1 / FT2
      };
      let targetCoefficientKey = `${fromUnit}to${this.unitConfig.outAreaUnit}`;
      if (isInArea) {
        targetCoefficientKey = `${fromUnit}to${this.unitConfig.inAreaUnit}in`;
      }
      const targetCoefficient = coefficient[targetCoefficientKey];
      let resArea = area;
      if (isReverse && targetCoefficient) {
        resArea = (area / targetCoefficient)?.toFixed(8);
        const [min = 0, max] = validator;
        if (resArea < min) {
          resArea = Math.ceil(resArea * 100) / 100;
        } else if (max?.toString?.() && resArea > max) {
          resArea = Math.floor(resArea * 100) / 100;
        }
      } else if (targetCoefficient) {
        resArea = fixed === 2 ? Math.round(area * targetCoefficient * 100) / 100 : (area * targetCoefficient).toFixed(fixed);
      } else {
        resArea = fixed === 2 ? Math.round(area * 100) / 100 : area.toFixed(fixed);
      }
      return resArea;
    },
    /**
     * 速度转换
     * @param {*} speed 速度
     * @param {*} fromUnit 传入的单位 默认 kmh 千米/时 可以为 kmh ms
     * @param {*} isReverse 是否逆向转化 默认否
     */
    unitTransfSpeed(speed = 0, fromUnit = 'kmh', isReverse = false) {
      // 米/秒 系数
      const MS = 3.6;
      // 英里/时 系数
      const MPH = 1.6093;
      const coefficient = {
        // 千米/时 转 米/秒
        kmhto2: MS,
        // 千米/时 转 英里/时
        kmlto3: 1 / MPH,
        // 米/秒 转 千米
        msto1: 1 / MS,
        // 米/秒 转 英里/时
        msto3: 1 / MS / MPH
      };
      let targetCoefficientKey = `${fromUnit}to${this.unitConfig.speedUnit}`;
      const targetCoefficient = coefficient[targetCoefficientKey];
      let resSpeed = speed;
      if (isReverse && targetCoefficient) {
        resSpeed = (speed / targetCoefficient)?.toFixed(8);
      } else if (targetCoefficient) {
        resSpeed = Math.round(speed * targetCoefficient * 100) / 100;
      } else {
        resSpeed = Math.round(speed * 100) / 100;
      }
      return resSpeed;
    },
    /**
     * 获取长度单位
     * @param {*} fromUnit 单位
     */
    getLengthUnit(fromUnit = 'km') {
      let resUnit = fromUnit;
      switch (this.unitConfig.lengthUnit) {
        case '2': {
          switch (fromUnit) {
            case 'm':
              resUnit = 'ft';
              break;
            case 'cm':
              resUnit = 'inch';
              break;
            default:
              resUnit = 'mile';
              break;
          }
          break;
        }
        default:
          break;
      }
      return resUnit;
    },
    /**
     * 长度转换
     * @param {*} length 长度
     * @param {*} fromUnit 传入的单位 默认 km 千米 可以为 km m cm
     * @param {*} isReverse 是否逆向转化 默认否
     * @param {*} validator 约束，可以填一个数字区域 [min, max]
     */
    unitTransfLength(
      length = 0,
      fromUnit = 'km',
      isReverse = false,
      validator = []
    ) {
      // 传出的单位 默认 mile 千米 可以为 mile ft inch
      // 英里 公里 系数
      const MILE = 1.6093;
      // 英尺 米 系数
      const FT = 0.3048;
      // 英寸 厘米 系数
      const INCH = 2.54;
      const coefficient = {
        // 公里 转 英里
        kmto2: 1 / MILE,
        // 米 转 英尺
        mto2: 1 / FT,
        // 厘米 转 英寸
        cmto2: 1 / INCH
      };
      let targetCoefficientKey = `${fromUnit}to${this.unitConfig.lengthUnit}`;
      const targetCoefficient = coefficient[targetCoefficientKey];
      let res = length;
      if (isReverse && targetCoefficient) {
        res = (length / targetCoefficient).toFixed(8);
        const [min = 0, max] = validator;
        if (res < min) {
          res = Math.ceil(res * 100) / 100;
        } else if (max?.toString?.() && res > max) {
          res = Math.floor(res * 100) / 100;
        }
      } else if (targetCoefficient) {
        res = Math.round(length * targetCoefficient * 100) / 100;
      } else {
        res = Math.round(length * 100) / 100;
      }
      return res;
    }
  }
};
