/**
 * 租户管理用 http
 */
import http from './http'
function sysHttp(data) {
  const options = {
    baseURL: process.env.VUE_APP_PERMISSION_API,
    headers: { 'x-platform-header': 'saas-farm' }
  }
  Object.assign(options, data)
  return http(options)
}
export default sysHttp
